package nl.kzaconnected.cursus.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.kzaconnected.cursus.CursusServiceApplication;
import nl.kzaconnected.cursus.model.Dao.*;
import nl.kzaconnected.cursus.model.Dto.CursusDto;
import nl.kzaconnected.cursus.repository.CursusRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = CursusServiceApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(
        classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CursusControllerIntegrationTest {

    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CursusRepository cursusRepository;

    private List<CursusDto> cursusDtos;
    private List<Cursus> cursussen;
    private Attitude attitude;
    private Slagingscriterium slagingscriterium;
    private Functieniveau functieniveau;
    private Status status;

    @Before
    public void init() throws Exception{
        attitude = Attitude.builder().id(1L).attitude("Test attitude").build();
        slagingscriterium = Slagingscriterium.builder().id(1L).slagingscriterium("Cursusavonden gevolgd").build();
        functieniveau = Functieniveau.builder().id(1L).functieniveau("Trainee").build();
        status = Status.builder().id(1L).status("Nog in te plannen").build();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        List<Datum> cursusData = new ArrayList<>();
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2018 08:00:00")).build());
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2019 09:00:00")).build());

        List<Docent> cursusDocenten = new ArrayList<>();
        cursusDocenten.add(Docent.builder().naam("Docent1").email("email1@email.nl").build());
        cursusDocenten.add(Docent.builder().naam("Docent2").email("email2@email.nl").build());

        List<Cursist> cursusCursisten = new ArrayList<>();
        cursusCursisten.add(Cursist.builder().naam("Cursist1").email("email1@email.nl").build());
        cursusCursisten.add(Cursist.builder().naam("Cursist2").email("email2@email.nl").build());

        CursusDto cursusDto1 = CursusDto.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .cursuscursisten(cursusCursisten)
                .maxdeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        CursusDto cursusDto2 = CursusDto.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .cursuscursisten(cursusCursisten)
                .maxdeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursusDtos = new ArrayList<>();
        cursusDtos.add(cursusDto1);
        cursusDtos.add(cursusDto2);

        Cursus cursus1 = Cursus.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .cursuscursisten(cursusCursisten)
                .maxDeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        Cursus cursus2 = Cursus.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .cursuscursisten(cursusCursisten)
                .maxDeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursussen = new ArrayList<>();
        cursussen.add(cursus1);
        cursussen.add(cursus2);
    }

    @Test
    public void getCursusByIdShouldReturn200OkAndJsonObject() throws Exception {
        cursusRepository.save(cursussen.get(0));
        mockMvc.perform(get("/api/cursussen/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(11)))
                .andExpect(jsonPath("$.id", is(cursussen.get(0).getId().intValue())))
                .andExpect(jsonPath("$.naam", is(cursussen.get(0).getNaam())))
                .andExpect(jsonPath("$.attitude", is("Test attitude")))
                .andExpect(jsonPath("$.slagingscriterium", is("Cursusavonden gevolgd")))
                .andExpect(jsonPath("$.functieniveau", is("Trainee")))
                .andExpect(jsonPath("$.status", is("Nog in te plannen")))
                .andExpect(jsonPath("$.maxdeelnemers", is(cursussen.get(0).getMaxDeelnemers())))
                .andExpect(jsonPath("$.beschrijving", is(cursussen.get(0).getBeschrijving())))
                .andExpect(jsonPath("$.cursusdata.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[0].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[1].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursusdocenten.[0].naam", is(cursussen.get(0).getCursusdocenten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[0].email", is(cursussen.get(0).getCursusdocenten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursusdocenten.[1].naam", is(cursussen.get(0).getCursusdocenten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[1].email", is(cursussen.get(0).getCursusdocenten().get(1).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursuscursisten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursuscursisten.[0].naam", is(cursussen.get(0).getCursuscursisten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[0].email", is(cursussen.get(0).getCursuscursisten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.[1].naam", is(cursussen.get(0).getCursuscursisten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[1].email", is(cursussen.get(0).getCursuscursisten().get(1).getEmail())));
    }

    @Test
    public void getAllCursussenShouldReturn200OkAndJsonArray() throws Exception {
        cursusRepository.save(cursussen.get(0));
        cursusRepository.save(cursussen.get(1));
        mockMvc.perform(get("/api/cursussen")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*", hasSize(11)))
                .andExpect(jsonPath("$[0].id", is(cursussen.get(0).getId().intValue())))
                .andExpect(jsonPath("$[0].naam", is(cursussen.get(0).getNaam())))
                .andExpect(jsonPath("$[0].attitude", is("Test attitude")))
                .andExpect(jsonPath("$[0].slagingscriterium", is("Cursusavonden gevolgd")))
                .andExpect(jsonPath("$[0].functieniveau", is("Trainee")))
                .andExpect(jsonPath("$[0].status", is("Nog in te plannen")))
                .andExpect(jsonPath("$[0].maxdeelnemers", is(cursussen.get(0).getMaxDeelnemers())))
                .andExpect(jsonPath("$[0].beschrijving", is(cursussen.get(0).getBeschrijving())))
                .andExpect(jsonPath("$[0].cursusdata.*", hasSize(2)))
                .andExpect(jsonPath("$[0].cursusdata.[0].*", hasSize(2)))
                .andExpect(jsonPath("$[0].cursusdata.[1].*", hasSize(2)))
                .andExpect(jsonPath("$[0].cursusdocenten.*", hasSize(2)))
                .andExpect(jsonPath("$[0].cursusdocenten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$[0].cursusdocenten.[0].naam", is(cursussen.get(1).getCursusdocenten().get(0).getNaam())))
                .andExpect(jsonPath("$[0].cursusdocenten.[0].email", is(cursussen.get(1).getCursusdocenten().get(0).getEmail())))
                .andExpect(jsonPath("$[0].cursusdocenten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$[0].cursusdocenten.[1].naam", is(cursussen.get(1).getCursusdocenten().get(1).getNaam())))
                .andExpect(jsonPath("$[0].cursusdocenten.[1].email", is(cursussen.get(1).getCursusdocenten().get(1).getEmail())))
                .andExpect(jsonPath("$[0].cursuscursisten.*", hasSize(2)))
                .andExpect(jsonPath("$[0].cursuscursisten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$[0].cursuscursisten.[0].naam", is(cursussen.get(1).getCursuscursisten().get(0).getNaam())))
                .andExpect(jsonPath("$[0].cursuscursisten.[0].email", is(cursussen.get(1).getCursuscursisten().get(0).getEmail())))
                .andExpect(jsonPath("$[0].cursuscursisten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$[0].cursuscursisten.[1].naam", is(cursussen.get(1).getCursuscursisten().get(1).getNaam())))
                .andExpect(jsonPath("$[0].cursuscursisten.[1].email", is(cursussen.get(1).getCursuscursisten().get(1).getEmail())))
                .andExpect(jsonPath("$[1].*", hasSize(11)))
                .andExpect(jsonPath("$[1].id", is(cursussen.get(1).getId().intValue())))
                .andExpect(jsonPath("$[1].naam", is(cursussen.get(1).getNaam())))
                .andExpect(jsonPath("$[1].attitude", is("Test attitude")))
                .andExpect(jsonPath("$[1].slagingscriterium", is("Cursusavonden gevolgd")))
                .andExpect(jsonPath("$[1].functieniveau", is("Trainee")))
                .andExpect(jsonPath("$[1].status", is("Nog in te plannen")))
                .andExpect(jsonPath("$[1].maxdeelnemers", is(cursussen.get(1).getMaxDeelnemers())))
                .andExpect(jsonPath("$[1].beschrijving", is(cursussen.get(1).getBeschrijving())))
                .andExpect(jsonPath("$[1].cursusdata.*", hasSize(2)))
                .andExpect(jsonPath("$[1].cursusdata.[0].*", hasSize(2)))
                .andExpect(jsonPath("$[1].cursusdata.[1].*", hasSize(2)))
                .andExpect(jsonPath("$[1].cursusdocenten.*", hasSize(2)))
                .andExpect(jsonPath("$[1].cursusdocenten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$[1].cursusdocenten.[0].naam", is(cursussen.get(1).getCursusdocenten().get(0).getNaam())))
                .andExpect(jsonPath("$[1].cursusdocenten.[0].email", is(cursussen.get(1).getCursusdocenten().get(0).getEmail())))
                .andExpect(jsonPath("$[1].cursusdocenten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$[1].cursusdocenten.[1].naam", is(cursussen.get(1).getCursusdocenten().get(1).getNaam())))
                .andExpect(jsonPath("$[1].cursusdocenten.[1].email", is(cursussen.get(1).getCursusdocenten().get(1).getEmail())))
                .andExpect(jsonPath("$[1].cursuscursisten.*", hasSize(2)))
                .andExpect(jsonPath("$[1].cursuscursisten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$[1].cursuscursisten.[0].naam", is(cursussen.get(1).getCursuscursisten().get(0).getNaam())))
                .andExpect(jsonPath("$[1].cursuscursisten.[0].email", is(cursussen.get(1).getCursuscursisten().get(0).getEmail())))
                .andExpect(jsonPath("$[1].cursuscursisten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$[1].cursuscursisten.[1].naam", is(cursussen.get(1).getCursuscursisten().get(1).getNaam())))
                .andExpect(jsonPath("$[1].cursuscursisten.[1].email", is(cursussen.get(1).getCursuscursisten().get(1).getEmail())));
    }

    @Test
    public void postCursusShouldReturn201CreatedAndJsonObject() throws Exception {
        String requestBody = mapper.writeValueAsString(cursusDtos.get(0));
        mockMvc.perform(post("/api/cursussen")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", hasSize(11)))
                .andExpect(jsonPath("$.id", is(cursussen.get(0).getId().intValue())))
                .andExpect(jsonPath("$.naam", is(cursussen.get(0).getNaam())))
                .andExpect(jsonPath("$.attitude", is("Test attitude")))
                .andExpect(jsonPath("$.slagingscriterium", is("Cursusavonden gevolgd")))
                .andExpect(jsonPath("$.functieniveau", is("Trainee")))
                .andExpect(jsonPath("$.status", is("Nog in te plannen")))
                .andExpect(jsonPath("$.maxdeelnemers", is(cursussen.get(0).getMaxDeelnemers())))
                .andExpect(jsonPath("$.beschrijving", is(cursussen.get(0).getBeschrijving())))
                .andExpect(jsonPath("$.cursusdata.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[0].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[1].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursusdocenten.[0].naam", is(cursussen.get(0).getCursusdocenten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[0].email", is(cursussen.get(0).getCursusdocenten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursusdocenten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$.cursusdocenten.[1].naam", is(cursussen.get(0).getCursusdocenten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[1].email", is(cursussen.get(0).getCursusdocenten().get(1).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursuscursisten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursuscursisten.[0].naam", is(cursussen.get(0).getCursuscursisten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[0].email", is(cursussen.get(0).getCursuscursisten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$.cursuscursisten.[1].naam", is(cursussen.get(0).getCursuscursisten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[1].email", is(cursussen.get(0).getCursuscursisten().get(1).getEmail())));
    }

    @Test
    public void putCursusShouldReturn200OkAndJsonObject() throws Exception {
        String requestBody = mapper.writeValueAsString(cursusDtos.get(1));
        cursusRepository.save(cursussen.get(0));
        mockMvc.perform(put("/api/cursussen/1")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(11)))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.naam", is(cursussen.get(1).getNaam())))
                .andExpect(jsonPath("$.attitude", is("Test attitude")))
                .andExpect(jsonPath("$.slagingscriterium", is("Cursusavonden gevolgd")))
                .andExpect(jsonPath("$.functieniveau", is("Trainee")))
                .andExpect(jsonPath("$.status", is("Nog in te plannen")))
                .andExpect(jsonPath("$.maxdeelnemers", is(cursussen.get(1).getMaxDeelnemers())))
                .andExpect(jsonPath("$.beschrijving", is(cursussen.get(1).getBeschrijving())))
                .andExpect(jsonPath("$.cursusdata.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[0].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdata.[1].*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursusdocenten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursusdocenten.[0].naam", is(cursussen.get(0).getCursusdocenten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[0].email", is(cursussen.get(0).getCursusdocenten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursusdocenten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$.cursusdocenten.[1].naam", is(cursussen.get(0).getCursusdocenten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursusdocenten.[1].email", is(cursussen.get(0).getCursusdocenten().get(1).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.*", hasSize(2)))
                .andExpect(jsonPath("$.cursuscursisten.[0].*", hasSize(3)))
                .andExpect(jsonPath("$.cursuscursisten.[0].naam", is(cursussen.get(0).getCursuscursisten().get(0).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[0].email", is(cursussen.get(0).getCursuscursisten().get(0).getEmail())))
                .andExpect(jsonPath("$.cursuscursisten.[1].*", hasSize(3)))
                .andExpect(jsonPath("$.cursuscursisten.[1].naam", is(cursussen.get(0).getCursuscursisten().get(1).getNaam())))
                .andExpect(jsonPath("$.cursuscursisten.[1].email", is(cursussen.get(0).getCursuscursisten().get(1).getEmail())));
    }

    @Test
    public void deleteCursusShouldReturn200Ok() throws Exception {
        cursusRepository.save(cursussen.get(0));
        mockMvc.perform(delete("/api/cursussen/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}