//package nl.kzaconnected.cursus.controller;
//
//import nl.kzaconnected.cursus.model.Dao.Cursist;
//import nl.kzaconnected.cursus.model.Dto.CursusDto;
//import nl.kzaconnected.cursus.service.CursusService;
//import nl.kzaconnected.cursus.service.EmailService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(AanmeldController.class)
//public class AanmeldControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//    @MockBean
//    private EmailService emailService;
//    @MockBean
//    private CursusService cursusService;
//
//    @Test
//    public void putAanmeldenCursis_ValidRequest_ShouldReturn200Ok() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "test@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        List<Cursist> listCursist = new ArrayList<>();
//        listCursist.add(Cursist.builder().naam(cursist).build());
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .naam(cursus)
//                .maxdeelnemers(2)
//                .cursuscursisten(listCursist)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenReturn(cursusDto);
//        Mockito.when(cursusService.save(cursusDto))
//                .thenReturn(cursusDto);
//        Mockito.when(emailService.sendAanmeldingMail(mailAdres, cursus, cursist))
//                .thenReturn(returnMsg);
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$").value("Succesvol aangemeld voor cursus " + cursus + ". " + returnMsg));
//    }
//
//    @Test
//    public void putAanmeldenCursis_InValidCursusId_ShouldReturn404NotFound() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "test@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenThrow(new NullPointerException());
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    public void putAanmeldenCursis_MaxDeelnemersIs0_ShouldReturn200Ok() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "test@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        List<Cursist> listCursist = new ArrayList<>();
//        listCursist.add(Cursist.builder().naam(cursist).build());
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .naam(cursus)
//                .maxdeelnemers(0)
//                .cursuscursisten(listCursist)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenReturn(cursusDto);
//        Mockito.when(cursusService.save(cursusDto))
//                .thenReturn(cursusDto);
//        Mockito.when(emailService.sendAanmeldingMail(mailAdres, cursus, cursist))
//                .thenReturn(returnMsg);
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$").value("Succesvol aangemeld voor cursus " + cursus + ". " + returnMsg));
//    }
//
//    @Test
//    public void putAanmeldenCursis_MaxDeelnemersIsSmallerOrEqualThanCursisten_ShouldReturn400BadRequest() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "test@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        List<Cursist> listCursist = new ArrayList<>();
//        listCursist.add(Cursist.builder().naam(cursist).build());
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .naam(cursus)
//                .maxdeelnemers(1)
//                .cursuscursisten(listCursist)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenReturn(cursusDto);
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$").value("Maximale aantal deelnemers is voor de cursus is bereikt"));
//    }
//
//    @Test
//    public void putAanmeldenCursis_InvalidEmailAdres_ShouldReturn400BadRequest() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        List<Cursist> listCursist = new ArrayList<>();
//        listCursist.add(Cursist.builder().naam(cursist).build());
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .naam(cursus)
//                .maxdeelnemers(0)
//                .cursuscursisten(listCursist)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenReturn(cursusDto);
//        Mockito.when(cursusService.save(cursusDto))
//                .thenReturn(cursusDto);
//        Mockito.when(emailService.sendAanmeldingMail(mailAdres, cursus, cursist))
//                .thenThrow(new IllegalArgumentException("Email adres moet geldig zijn!"));
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$").value("Email adres moet geldig zijn!"));
//    }
//
//    @Test
//    public void putAanmeldenCursis_EmailServerFault_ShouldReturn500InternalServerError() throws Exception {
//        String returnMsg = "Mail is succesvol verzonden!";
//        String mailAdres = "test@test.nl";
//        String cursus = "cursus";
//        String cursist = "cursist";
//        Long id = 1L;
//        List<Cursist> listCursist = new ArrayList<>();
//        listCursist.add(Cursist.builder().naam(cursist).build());
//        CursusDto cursusDto = CursusDto.builder()
//                .id(id)
//                .naam(cursus)
//                .maxdeelnemers(0)
//                .cursuscursisten(listCursist)
//                .build();
//        Mockito.when(cursusService.findOne(1L))
//                .thenReturn(cursusDto);
//        Mockito.when(cursusService.save(cursusDto))
//                .thenReturn(cursusDto);
//        Mockito.when(emailService.sendAanmeldingMail(mailAdres, cursus, cursist))
//                .thenThrow(new RuntimeException("Mail is niet succesvol verzonden."));
//        mockMvc.perform(put("/api/aanmelden/{id}/{cursist}/{email}", id, cursist,  mailAdres)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isInternalServerError())
//                .andExpect(jsonPath("$").value("Succesvol aangemeld voor cursus "
//                        + cursus
//                        + ". Maar het versturen van mail is mislukt met melding: "
//                        + "Mail is niet succesvol verzonden."));
//    }
//}
