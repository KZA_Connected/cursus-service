package nl.kzaconnected.cursus.service.Mail;


import nl.kzaconnected.cursus.model.Dao.Cursist;
import nl.kzaconnected.cursus.model.Dao.Docent;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import nl.kzaconnected.cursus.service.EmailService;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @Rule
    public SmtpServerRule smtpServerRule = new SmtpServerRule(2525);

    @Value("${spring.mail.mainemailadres}")
    private String mainEmailAdres;

    @Test
    public void shouldSendAanmeldingMail() throws MessagingException, IOException {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("test@mail.nl").build();
        String returnMsg = "";

        try {
            returnMsg = emailService.sendAanmeldingMail(cursus, cursist);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals("Mail is succesvol verzonden.", returnMsg);

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(2, receivedMessages.length);

        MimeMessage currentMail = receivedMessages[0];
        assertEquals("Aanmelding cursus", currentMail.getSubject());
        assertEquals(mainEmailAdres, currentMail.getAllRecipients()[0].toString());
        assertEquals(cursist.getEmail(), currentMail.getAllRecipients()[1].toString());
        assertTrue(String.valueOf(currentMail.getContent()).contains("Hallo Chantal"));
        assertTrue(String.valueOf(currentMail.getContent()).contains("Graag wil ik mij aanmelden voor de volgende cursus:"));
        assertTrue(String.valueOf(currentMail.getContent()).contains(cursus));
        assertTrue(String.valueOf(currentMail.getContent()).contains("Gr.,"));
        assertTrue(String.valueOf(currentMail.getContent()).contains(cursist.getNaam()));

        // Check if cc is same as to
        assertEquals(currentMail.getSubject(), receivedMessages[1].getSubject());
        assertEquals(currentMail.getAllRecipients(), receivedMessages[1].getAllRecipients());
        assertEquals(currentMail.getContent(), receivedMessages[1].getContent());
    }

    @Test
    public void shouldSendNewCursusMail() throws Exception {
        List<Docent> cursusdocenten = new ArrayList<>();

        Docent docent1 = Docent.builder().id(1L).naam("docentNaam1").email("test1@kza.nl").build();
        Docent docent2 = Docent.builder().id(2L).naam("docentNaam2").email("test2@kza.nl").build();

        cursusdocenten.add(docent1);
        cursusdocenten.add(docent2);

        emailService.sendNewCursusMail(cursusdocenten);

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(2, receivedMessages.length);

        MimeMessage currentMail1 = receivedMessages[0];
        assertEquals("Opzetten nieuwe cursus", currentMail1.getSubject());
        assertEquals(1, currentMail1.getAllRecipients().length);
        assertEquals(docent1.getEmail(), currentMail1.getAllRecipients()[0].toString());
        assertTrue(String.valueOf(currentMail1.getContent()).contains("Beste Docent,"));
        assertTrue(String.valueOf(currentMail1.getContent()).contains("Binnenkort ga je voor een groep KZA'ers een training verzorgen."));

        MimeMessage currentMail2 = receivedMessages[1];
        assertEquals("Opzetten nieuwe cursus", currentMail2.getSubject());
        assertEquals(1, currentMail1.getAllRecipients().length);
        assertEquals(docent2.getEmail(), currentMail2.getAllRecipients()[0].toString());
        assertTrue(String.valueOf(currentMail2.getContent()).contains("Daarom vragen we je om een paar zaken te doen:"));
        assertTrue(String.valueOf(currentMail2.getContent()).contains("Met vriendelijke groet,"));
        assertTrue(String.valueOf(currentMail2.getContent()).contains("Chantal Hogerhuis"));
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsInvalid() {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("invalid@.nl").build();

        try {
            emailService.sendAanmeldingMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsEmptyString() {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").email("").build();

        try {
            emailService.sendAanmeldingMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsNull() {
        String cursus = "cursusNaam";
        Cursist cursist = Cursist.builder().id(1L).naam("cursistNaam").build();

        try {
            emailService.sendAanmeldingMail(cursus, cursist);
        } catch (Exception e) {
            assertEquals("Email adres moet geldig zijn.", e.getMessage());
        }

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }

    @Test
    public void shouldNotSendMailWithMailAdresIsNotKZA() throws Exception {
        List<Docent> cursusdocenten = new ArrayList<>();

        Docent docent1 = Docent.builder().id(1L).naam("docentNaam1").email("test1@mail.nl").build();
        cursusdocenten.add(docent1);

        emailService.sendNewCursusMail(cursusdocenten);

        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(0, receivedMessages.length);
    }
}
