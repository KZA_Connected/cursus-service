package nl.kzaconnected.cursus.controller;

import nl.kzaconnected.cursus.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursus.service.SlagingscriteriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/slagingscriteria")
@CrossOrigin
public class SlagingscriteriumController {

    @Autowired
    private SlagingscriteriumService slagingscriteriumService;

    @GetMapping
    public List<Slagingscriterium> getAllSlagingscriteria() {
        return slagingscriteriumService.findAll();
    }
}


