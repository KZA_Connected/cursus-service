package nl.kzaconnected.cursus.controller;

import nl.kzaconnected.cursus.model.Dao.Functieniveau;
import nl.kzaconnected.cursus.service.FunctieniveauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/functieniveaus")
@CrossOrigin
public class FunctieniveauController {

    @Autowired
    private FunctieniveauService functieniveauService;

    @GetMapping
    public List<Functieniveau> getAllfunctieniveaus() {
        return functieniveauService.findAll();
    }
}


