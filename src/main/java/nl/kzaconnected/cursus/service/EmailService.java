package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Cursist;
import nl.kzaconnected.cursus.model.Dao.Docent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.List;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.mainemailadres}")
    private String mainEmailAdres;

    @Value("${spring.mail.senderemailadres}")
    private String senderEmailAdres;

    public String sendAanmeldingMail(String cursusNaam, Cursist cursist) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        if (isValidMail(cursist.getEmail())) {

            helper.setFrom(senderEmailAdres);
            helper.setTo(mainEmailAdres);
            helper.setCc(cursist.getEmail());
            helper.setSubject("Aanmelding cursus");
            helper.setText("Hallo Chantal," +
                    "\n \n Graag wil ik mij aanmelden voor de volgende cursus:" +
                    "\n " + cursusNaam +
                    "\n \n Gr.," +
                    "\n " + cursist.getNaam());
            try {
                this.sender.send(message);
                return "Mail is succesvol verzonden.";
            } catch (Exception ex) {
                throw new RuntimeException("Mail is niet succesvol verzonden. " + ex);
            }
        } else {
            throw new IllegalArgumentException("Email adres moet geldig zijn.");
        }
    }

    public void sendNewCursusMail(List<Docent> cursusdocenten) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        for (Docent cursusdocent : cursusdocenten) {

            if (isValidMail(cursusdocent.getEmail()) && cursusdocent.getEmail().contains("@kza.nl")) {

                helper.setFrom(senderEmailAdres);
                helper.setTo(cursusdocent.getEmail());
                helper.setSubject("Opzetten nieuwe cursus");
                helper.setText("Beste Docent," +
                        "\n\nBinnenkort ga je voor een groep KZA'ers een training verzorgen. " +
                        "We willen graag een aantal zaken daarvan bijhouden zodat we ook de trainingen en het proces eromheen verder kunnen verbeteren." +
                        "\nDaarom vragen we je om een paar zaken te doen:" +
                        "\n\n * Presentielijst bijhouden" +
                        "\n * Evaluatie laten invullen en verzamelen" +
                        "\n * Eigen goede en verbeterpunten noteren" +
                        "\n * Docenten declaratieformulier" +
                        "\n\nBovenstaande documenten mag je aanleveren bij mij." +
                        "\nVoor het gemak heb ik in onderstaande links de documenten die je nodig hebt al toegevoegd." +
                        "\nVoor vragen weet je mij te vinden." +
                        "\n\nSucces en vooral veel plezier!" +
                        "\n\nMet vriendelijke groet," +
                        "\nChantal Hogerhuis" +
                        "\n\nBijlage:" +
                        "\n * Presentielijst: https://kza.sharepoint.com/:w:/r/Docbibiblio%201/KZA%20Presentielijst.docx?d=wefb630606edd4722a47ab8d83a76098f&csf=1" +
                        "\n * Evaluatieformulier: https://kza.sharepoint.com/:w:/r/Docbibiblio%201/KZA%20Evaluatieformulier.doc?d=w60bd4db9dd944bc381c2cef0fdfc9b2c&csf=1" +
                        "\n * Declaratieformulier: https://kza.sharepoint.com/:w:/r/Docbibiblio%201/KZA%20Declaratieformulier%20Docenten%20Avondcursus.docx?d=w508b83d5798d4a9db7cafb7d71c2110d&csf=1");
                this.sender.send(message);
            }
        }
    }

    private boolean isValidMail(String mailAdres) {
        return EmailValidator.getInstance().isValid(mailAdres);
    }
}
