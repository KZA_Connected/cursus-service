#!groovy
def appName = "cursusservice"
def project = "hco-kza-connected"
def appTagO = "ontwikkel"
//def appTagS = "staging"
def appTagP = "productie"
def majorVersion = "1"
def imageVersion = ""

pipeline {
    agent {
        label "maven"
    }
    options {
        skipDefaultCheckout()
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')
    }

    stages {
        stage ('Check out') {
            steps {
                checkout scm
                stash(name: 'ws', includes: "**", excludes: '**/.git/**')
                stash(name: 'openshift', includes: '**/openshift/**')
            }
        }
        stage ('Build code') {
            steps {
                unstash 'ws'
                sh(script: "mvn clean package -Pfatjar -DskipTests")
                stash(name: 'fatjar', includes: '**/target/fatjar.jar')
            }
        }
        stage ('Run unit tests') {
            steps {
                unstash 'ws'
                sh(script: "mvn clean test")
            }
            post {
                always {
                    junit '**/surefire-reports/**/*.xml'
                }
            }
        }
        stage('Create build config'){
            when {
                expression {
                    !resourceExist(
                            project: project,
                            appName: appName,
                            resource: 'bc'
                    )
                }
            }
            steps {
                unstash 'openshift'
                createBuildConfig(
                        project: project,
                        appName: appName
                )
            }
        }
        stage('Build image') {
            steps {
                unstash 'fatjar'
                buildOpenshiftImage(
                        project: project,
                        appName: appName
                )
            }
        }
        stage('Create deployment config'){
            when {
                expression {
                    !resourceExist(
                            project: project,
                            appName: "${appName}-${appTagO}",
                            resource: 'dc'
                    )
                }
            }
            steps {
                unstash 'openshift'
                createDeploymentConfig(
                        project: project,
                        appName: appName,
                        envVar0: 'MAIL_MAINEMAILADRES=kzaconnected@kza.nl',
                        envVar1: "IMAGE=${project}/${appName}:latest",
                        envVar2: "APPTAG=${appTagO}"
                )
            }
        }
        stage('Deploy image to ontwikkel') {
            steps {
                unstash 'openshift'
                triggerDeployment(
                        project: project,
                        appName: appName,
                        appTag: appTagO
                )
            }
        }
        stage('Run end 2 end tests') {
            steps {
                echo "To do!!"
            }
        }
        stage('Tag images') {
            when {
                branch 'master'
            }
            steps {
                script {
                    imageVersion = setImageVersion(majorVersion)
                }
                pushImageTagFromToOpenShiftProject(
                        fromProject: project,
                        toProject: project,
                        appName: appName,
                        fromTag: 'latest',
                        toTag: imageVersion
                )
            }
        }
        /* stage('Deploy new image to staging'){
            when {
                branch 'master'
            }
            steps {
                updateDeploymentConfig(
                        project: project,
                        appName: appName,
                        imageTag: imageVersion,
                        appTag: appTagS
                )
                triggerDeployment(
                        project: project,
                        appName: appName,
                        appTag: appTagS
                )
            }
        } */ 
        stage('Run end 2 end test on staging') {
            when {
                branch 'master'
            }
            steps {
                echo 'To do'
            }
        }
        stage('Deploy new image to production'){
            when {
                branch 'master'
            }
            steps {
                updateDeploymentConfig(
                        project: project,
                        appName: appName,
                        imageTag: imageVersion,
                        appTag: appTagP
                )
                triggerDeployment(
                        project: project,
                        appName: appName,
                        appTag: appTagP
                )
            }
        }
    }
}
